<section id="jawaban">
    <div class="container mt-5 mb-5">
        <div class="card">
            <div class="card-header d-flex justify-content-between align-items-center bg-white mx-4 mt-4">
                <h5 class="mb-0">Jawaban Pertanyaan Anda</h5>
            </div>
            @if($question)
            <div class="card-body mt-4">
                <div class=" mx-4 mb-4 card">
                    <a href="#" style="text-decoration: none; color: inherit;">
                        <div class="px-4 ">
                            <table style="width: 100%;">
                                <tr>
                                    <td class="p-4 col-lg-2" rowspan="3">
                                        <img src="img/profile.jpg" alt="Profile Image" id="profileImage" class="rounded-circle" height="45" width="45" data-toggle="dropdown">
                                    </td>
                                    <td class="col-lg-10">
                                            <div class="mt-4 mx-4">
                                                <h6>{{ $question->user->nama }}</h6>
                                                    <p>{{ $question->question_content }}</p>
                                                <div class="text-muted d-flex justify-content-end">
                                                    <p>{{ $question->created_at->format('j F Y') }}</p>
                                                </div>
                                            </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </a>
                    <a href="#" style="text-decoration: none; color: inherit;">
                        <div class="px-4 bg-light">
                            <table style="width: 100%;">
                                <tr>
                                    <td class="p-4 col-lg-2" rowspan="3">
                                        <img src="img/profile.jpg" alt="Profile Image" id="profileImage" class="rounded-circle" height="45" width="45" data-toggle="dropdown">
                                    </td>
                                    <td class="col-lg-10">
                                        <div class="mt-4 mx-4">
                                            <h6>{{ $question->admin->nama ?? 'Nama ADMIN' }}</h6>
                                            <p>{{ $question->answer_content }}</p>
                                            <div class="text-muted d-flex justify-content-end">
                                                <p>{{ $question->updated_at->format('j F Y') }}</p>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </a>
                </div>

            </div>
            @else
            <div class="mt-4 mx-4">
                <h6>Tidak ada pertanyaan dari user ini</h6>
            </div>
            @endif
        </div>
    </div>
</section>
